package com.shamrock.oauth.api.service;


public class Main {
	public static void main(String[] args) {
		OAuthApiService qq = new QQOAuthApi();
		//qq登录授权
		System.out.println(qq.authorize());
		//授权后获取用户信息
		//qq.getUser(code);
		OAuthApiService sina = new SinaWeiboOAuthApi();
		//新浪微博登录授权
		System.out.println(sina.authorize());
		//授权后获取用户信息
		//sina.getUser(code);
		OAuthApiService weixin = new WeiXinOAuthApi();
		//微信登录授权
		System.out.println(weixin.authorize());
		//授权后获取用户信息
		//weixin.getUser(code);
		
	}
}
